﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Lab6_Dictionary
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent(); //call the function here
        }

        async void GeneratePictureButton_Clicked(object sender, EventArgs e)
        {
            string owlbotApiEndpoint = "https://owlbot.info/api/v2/dictionary/";
            Uri owlbotApiUri = new Uri(owlbotApiEndpoint);

            HttpClient client = new HttpClient();
            OwlBotData owlData = new OwlBotData();

            HttpResponseMessage respond = await client.GetAsync(owlbotApiUri);

            string jsonContent = await respond.Content.ReadAsStringAsync();

            owlData = JsonConvert.DeserializeObject<owlData>(jsonContent);

            owlImage.Source = owlData.Message;
        }
    }
}
